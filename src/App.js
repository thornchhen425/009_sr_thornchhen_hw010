import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Container } from "react-bootstrap";
import Menu from "./components/Menu";
import Post from "./pages/Post";
import User from "./pages/User";
import Author from "./pages/Author";

export default function App() {
  return (
    <BrowserRouter>
      <Menu />
      <Container>
        <Switch>
          <Route path="/author" component={Author} />
          <Route path="/post" component={Post} />
          <Route path="/user" component={User} />
          <Route path="*" render={()=> <h1>404 Not Found</h1>}/>
        </Switch>
      </Container>
    </BrowserRouter>
  );
}
