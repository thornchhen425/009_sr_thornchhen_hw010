import React, { useState, useEffect, useImperativeHandle } from "react";
import { Container, Table, Button, Row, Col, Form } from "react-bootstrap";
import {
  deleteAuthor,
  fetchAuthor,
  postAuthor,
  updateAuthorById,
  uploadImage,
} from "../service/Author_Service";

export default function Author() {
  const [authors, setAuthor] = useState([]);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [imageURL, setImageURL] = useState(
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARIAAAC4CAMAAAAYGZMtAAAAdVBMVEVpXM2HfdFeUc7/8rbdyr7/7LiFe9H/87ZZTM9mWc1kV87/8LedjsdiVc5xZMzn1L6hlM1/ddKtncTz37qyosTWw7+XjM5+ccr96LmUhciGeMrXxb7/97VrXs1dUM6tn8p6cdPLucDiz72Rhs90bNTv27q6qcNOvLJNAAABwUlEQVR4nO3bXXPSQBiAUUyMdBPSKh9FpB+olf//EwUGCs72YsNY0Ow5V9wwszyThJdhdzAAAAAAAAAAAAAAAAAAAAAAAPjvtEUX7bWXewH1+GMX0xyaTJoyXTW8ufZ6L2DSPH9Odf81jyTl/adUq3ySfEgkiSSS7EkSkSQiyUFoQ9hNopLshUVVNd+3TSTZa6dNNZxJcmKTZDlw45xqp+Ws2L2SZE+SiCQRSSKSRCQ5CnVdB0lOhPloNFrUkhw9VU1TPiyCJK9uhtXd3Y9bV8nRJsmqKDxLTmySzDdPV0mOJIlIEpEk8laSn19SvWSSZPOtnKyPf5NHScKvDhsHyqZ6uvIH+PviJNsJP93uzf0S3zjb34Hp+lfkjSQpQp/3Hp2VJLyMH99xTVd2VpJi9jDu73VyZpKyx9v2JImcJkkeMZ4ySdIsB7eJBssmjyRVk6zKIcl2M0UXix7OaHuHJJuhNXTQx6n14DUJB92SZHGwolOSx+U6gyZdkoRvw8m7Lubf0C1JKcmf8kmyKto0xTyXJOtxoud1k0mS9INrTRZJ2mmn043j+toLvgBnYAEAAAAAAAAAAAAAAAAAAACA3vsNEW1B3Y++7NIAAAAASUVORK5CYII="
  );
  const [imageFile, setImageFile] = useState(null);
  const [id, setId] = useState("");
  //Fetch all authors from Author_Service
  useEffect(() => {
    const fetch = async () => {
      let response = await fetchAuthor();
      setAuthor(response);
    };
    fetch();
  }, []);

  //Delete author by ID
  const onDelete = (id) => {
    deleteAuthor(id)
      .then((message) => {
        let newAuthors = authors.filter((authors) => authors._id !== id);
        setAuthor(newAuthors);
        alert(message);
      })
      .catch((e) => console.log(e));
  };

  //Post athour to API
  const onAdd = async (e) => {
    e.preventDefault();

    let author = {
      name,
      email,
    };
    if (imageFile) {
      let url = await uploadImage(imageFile);
      author.image = url;
    }
    postAuthor(author).then((message) => alert(message));
  };

  //Update author by ID
  const onUpdate = (author) => {
    document.getElementById("name").value = author.name;
    document.getElementById("email").value = author.email;
    setImageURL(author.image);
    setId(author._id);
    setName(author.name);
    setEmail(author.email);
  };

  const onUpdateAuthor = async (id) => {
    const url = imageFile && (await uploadImage(imageFile));
    const temp = {
      name,
      email,
      image: url ? url : imageURL,
    };
    updateAuthorById(id, temp).then((message) => alert(message))
  };

  return (
    <Container>
      <h1 className="my-4">Author</h1>

      <Row className="my-4">
        <Col md={8}>
          <Form>
            <Form.Group>
              <Form.Label>Author Name</Form.Label>
              <Form.Control
                id="name"
                type="text"
                placeholder="Author Name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control
                id="email"
                type="email"
                placeholder="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>
            <Button variant="primary" onClick={onAdd}>
              Add
            </Button>{' '}
            <Button variant="success" onClick={() => onUpdateAuthor(id)}>
              Save
            </Button>
          </Form>
        </Col>

        <Col md={4}>
          <Form>
            <img
              className="w-100"
              src={
                imageURL
                  ? imageURL
                  : "https://lh3.googleusercontent.com/proxy/1QncqUzWyyg8QJZHWW2O_rZ0Z_Wm0t8bFluu6fERSDLnYiG6b5l-nz5W2XKD3QH79T677qFQqbq_kpP0h6Yq-cTbnUQehS-ga2cDhC9HHWXRs3RciUDYrQNurDw"
              }
            />
            <Form.Group>
              <Form.File
                id="img"
                label="Choose Image"
                onChange={(e) => {
                  let url = URL.createObjectURL(e.target.files[0]);
                  setImageFile(e.target.files[0]);
                  setImageURL(url);
                }}
              />
            </Form.Group>
          </Form>
        </Col>
      </Row>

      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Image</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {authors.map((author, index) => {
            return (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>{author.name}</td>
                <td>{author.email}</td>
                <td>
                  <img width="200px" src={author.image} alt="" />
                </td>
                <td>
                  <Button variant="primary">View</Button>{" "}
                  <Button variant="warning" onClick={() => onUpdate(author)}>
                    Edit
                  </Button>{" "}
                  <Button variant="danger" onClick={() => onDelete(author._id)}>
                    Delete
                  </Button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </Container>
  );
}
