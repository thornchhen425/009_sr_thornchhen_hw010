import API from "../API/API"

//Fetch all authors from API
export const fetchAuthor = async () => {
    let response = await API.get('author')
    return response.data.data
}

//Delete author by ID
export const deleteAuthor = async (id) => {
    let response = await API.delete('author/' + id)
    return response.data.message
}

//Post author to API
export const postAuthor = async (author) => {
    let response = await API.post('author', author)
    return response.data.message
}

//Upload image to API
export const uploadImage = async (file) => {

    let formData = new FormData()
    formData.append('image', file)

    let response = await API.post('images', formData)
    return response.data.url
}

//Update author by ID
export const updateAuthorById = async(id, newAuthor) => {
    let response = await API.put('author/' + id, newAuthor)
    return response.data.message
}

